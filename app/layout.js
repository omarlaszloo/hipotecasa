import './globals.css'
import { Inter } from 'next/font/google'
import { Navigation, Footer } from '@/components/'
import "@fortawesome/fontawesome-svg-core/styles.css";
import { config } from "@fortawesome/fontawesome-svg-core";
import { ListMain } from '../components/ListMain/ListMain';
import { GoogleAnalytics } from '@next/third-parties/google'
config.autoAddCss = false;

import styles from "./page.module.css"

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'Applicasas',
  description: 'Hipoteca digital',
}


export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <head>
      </head>
      <body>
        <div className={styles.main}>
          <Navigation />
          <ListMain />
        </div>
        {children}
        <Footer />
        <GoogleAnalytics gaId="G-0GJZ60GDRH" />
      </body>
    </html>
  )
}
