import { Tools, Contacts, Banks, Products, Requirements } from '@/components'
export const dynamic = "force-dynamic";


export default function Homepage() {
  return (
    <>
      <Tools />
      <Requirements />
      <Products />
      <Banks />
      <Contacts />
    </>
  )
}