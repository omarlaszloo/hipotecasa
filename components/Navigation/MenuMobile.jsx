'use client'
import React, { useState } from 'react'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faWandMagicSparkles,
  faCircleXmark,
} from "@fortawesome/free-solid-svg-icons"
import styles from './Navigation.module.css'
import nav from './contants/nav'

export default function MenuMobile({ setIsMenu, isMenu }) {

  return (
    <div className={styles.menuMobile}>
      <div className={styles.contentMenuMobile} />
      <div className={styles.mainMobile}>
        <div className={styles.contentClose}>
          <div className={styles.iconClose} onClick={() => setIsMenu(!isMenu)}>
            <FontAwesomeIcon
              icon={faCircleXmark}
              className="fas fa-check"
              style={{ color: '#fff', fontSize: 25, display: 'flex', justifyContent: 'end' }}
            />
          </div>
        </div>
        <div className={styles.menuMobileSimulator}>
          <button
            className={styles.btnSimulator}>
            <FontAwesomeIcon
              icon={faWandMagicSparkles}
              className="fas fa-check"
              style={{ color: '#fff', fontSize: 15, marginLeft: '10px', marginRight: '10px' }}
            />
            <spam>SIMULADOR</spam>
          </button>
        </div>
        <ul className={styles.navigationMobile}>
          {nav?.map(({ label, route, icon }) => (
            <li>
              <div className={styles.contentListMobile} onClick={() => setIsMenu(!isMenu)}>
                <div className={styles.iconMobile}>
                  <FontAwesomeIcon
                    icon={icon}
                    className="fas fa-check"
                    style={{ color: '#fff', fontSize: 25, marginLeft: '15px', marginRight: '20px' }}
                  />
                </div>
                <div className={styles.itemMenu}>
                  <Link activeClass="active" smooth spy={true} to={label} href={`#${label}`} duration={500} offset={-70}>
                    {label}
                  </Link>
                </div>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}