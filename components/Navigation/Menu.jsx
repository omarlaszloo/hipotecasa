
'use client'

import React, { useEffect, useState } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faWandMagicSparkles,
  faBars,
} from "@fortawesome/free-solid-svg-icons"
import styles from './Navigation.module.css'
import nav from './contants/nav'
import iconSocial from './contants/iconSocial'
import logo from './assets/images/output-logo.png'
import MenuMobile from './MenuMobile'

export default function Menu({ social = [], theme = [] }) {
  const [isMenu, setIsMenu] = useState(false)
  const [scrollY, setScrollY] = useState(0)
  const [isScrollMenu, setIsScrollMenu] = useState(0)
  const scroll = 112

  useEffect(() => {
    const handleScroll = () => {
      setScrollY(window.scrollY)
    }

    handleScroll()

    window.addEventListener("scroll", handleScroll)
    return () => {
      window.removeEventListener("scroll", handleScroll)
    }

  }, [])

  useEffect(() => {
    if (scrollY > 112) {
      setIsScrollMenu(scrollY)
    } else {
      setIsScrollMenu(0)
    }
  }, [scrollY])

  const itemsNav = (items) => (
    <nav className={styles.nav}>
      <div className={styles.logo}>
        <Image src={logo} className={styles.logoMenu} />
      </div>
      <div className={styles.social}>
        <ul>
          {social?.map((item, index) => {
            const filterIcon = iconSocial.filter(itemIcon => itemIcon?.name === item?.name)[0]
            return (
              <li className={styles.iconSocial}>
                {item?.name === 'phone' ? (
                  <a href={`tel:${item?.link}`}>
                    {filterIcon?.icon}
                  </a>
                )
                  :
                  <Link href={item?.link} target="_blank" legacyBehavior>
                    {filterIcon?.icon}
                  </Link>
                }
              </li>
            )
          })}
        </ul>
      </div>
      <div className={styles.menu}>
        <ul className={styles.navigation}>
          {items?.map(({ label, route }) => (
            <li>
              <Link activeClass="active" smooth spy={true} to={label} href={route} duration={500} offset={-70}>
                {label}
              </Link>
            </li>
          ))}
        </ul>
      </div>
      {/* button menu movile */}
      <div className={styles.menuBars} onClick={() => setIsMenu(!isMenu)}>
        <FontAwesomeIcon
          icon={faBars}
          className="fas fa-check"
          style={{ color: '#4070AB', fontSize: 30 }}
        />
      </div>
      <div className={styles.simulator}>
        <div className={styles.contenSimulator}>
          <button
            className="btn btn-neutral btn-xs sm:btn-xs md:btn-xs lg:btn-sm">
            <FontAwesomeIcon
              icon={faWandMagicSparkles}
              className="fas fa-check"
              style={{ color: '#fff', fontSize: 15, marginLeft: '10px', marginRight: '10px' }}
            />
            <spam>SIMULADOR</spam>
          </button>
        </div>
      </div>
    </nav>
  )

  const floatMenu = (items) => {
    return (
      <nav className={styles.floatNav}>
        <div className={styles.floatLogo}>
          <Image src={logo} className={styles.floatLogoMenu} />
        </div>
        <div className={styles.social}>
          <ul>
            {social?.map((item, index) => {
              const filterIcon = iconSocial.filter(itemIcon => itemIcon.name === item.name)[0]
              return (
                <li className={styles.iconSocial}>
                  {item?.name === 'phone' ? (
                    <a href={`tel:${item.link}`}>
                      {filterIcon?.icon}
                    </a>
                  ) :
                    <Link href={item?.link} target="_blank">
                      <div>{filterIcon?.icon}</div>
                    </Link>}
                </li>
              )
            })}
          </ul>
        </div>
        <div className={styles.floatMenu}>
          <ul className={styles.navigation}>
            {items?.map(({ label, route }) => (
              <li>
                <Link activeClass="active" smooth spy={true} to={label} href={route} duration={500} offset={-70}>
                  {label}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </nav>
    )
  }

  return (
    <>
      {isMenu ? <MenuMobile setIsMenu={setIsMenu} isMenu={isMenu} /> : null}
      <header className={styles.header} style={{ backgroundColor: theme[0]?.headerColor }}>
        {itemsNav(nav)}
      </header>

      {isScrollMenu > scroll ?
        <header className={styles.headerScroll}>
          {floatMenu(nav)}
        </header> : null}
    </>
  )
}
