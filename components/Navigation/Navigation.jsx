import Menu from './Menu'
import fetData from '../../utils/AxiosServices'

export default async function Navigation() {
  const social = await fetData('social/')
  const DOMAIN_PROD = 'https://octopus-app-mpo3n.ondigitalocean.app/'
  const response = await fetch(`${DOMAIN_PROD}theme/`);
  const theme = await response.json();

  return <Menu social={social} theme={theme} />
}




