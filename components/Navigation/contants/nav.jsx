import {
  faGear,
  faFileContract,
  faLandmark,
  faCubes,
  faBlog,
  faAddressBook
} from '@fortawesome/free-solid-svg-icons';

const nav = [
  {
    label: 'Productos',
    route: '/#Productos',
    icon: faCubes,
  },

  {
    label: 'Herramientas',
    route: '/#Herramientas',
    icon: faGear
  },
  {
    label: 'Requisitos',
    route: '/',
    icon: faFileContract,
  },
  {
    label: 'Bancos',
    route: '/#Bancos',
    icon: faLandmark,
  },
  {
    label: 'blog',
    route: '/blog',
    icon: faBlog,
  },
  {
    label: 'Contacto',
    route: '/',
    icon: faAddressBook,
  }
]

export default nav;
