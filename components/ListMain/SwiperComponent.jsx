'use client'

import React from 'react';
import styles from './main.module.css'
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

import { Autoplay, Pagination, Navigation } from 'swiper/modules';

export default function SwiperComponent({ listMain, theme = [] }) {
  return (
    <div className={styles.contentSwiper} style={{ backgroundColor: theme[0]?.headerColor }}>
      <Swiper
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 9500,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Autoplay, Pagination, Navigation]}
      >

        {listMain?.map(({ title, description }) => (
          <SwiperSlide className={styles.swiper}>
            <div className={styles.mainDescription}>
              <div className={styles.content}>
                <p className={styles.title} style={{ color: theme[0]?.tileColor, fontSize: `${Number(theme[0]?.titleSize)}vw` }}>
                  {title}
                </p>
                <p className={styles.mainSubtitle} style={{ color: theme[0]?.subTitleColor, fontSize: `${Number(theme[0]?.subTitleSize)}vw` }}>
                  {description}
                </p>
              </div>
            </div>
          </SwiperSlide>
        ))
        }
      </Swiper>
    </div>
  )
}