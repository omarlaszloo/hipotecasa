import SwiperComponent from './SwiperComponent'
import fetData from '../../utils/AxiosServices'

const { ADMIN_API_APPLICASA_PROD } = process.env;

export async function ListMain() {
  const DOMAIN_PROD = 'https://octopus-app-mpo3n.ondigitalocean.app/'
  const response = await fetch(`${DOMAIN_PROD}theme/`);
  const theme = await response.json();
  const main = await fetch(`${DOMAIN_PROD}main/`);
  
  const listMain = await main.json();
  return <SwiperComponent listMain={listMain} theme={theme} />
}