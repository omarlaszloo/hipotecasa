'use client'

import React, { useState } from 'react'
import imagenCustom from '../../app/assets/images/pexels-karolina-grabowska-5900074.jpg'
import styles from './contentBlog.module.css'


export default function BlogComponent({ data }) {
  const [showArticle, setShowArticle] = useState(data[0]);

  const FullContentBlog = ({ data }) => {
    console.log(':: data', data)
    return (
      <>
        <article className={styles.title}>
          <div className={styles.imgBg} style={{ backgroundImage: `url(https://octopus-app-mpo3n.ondigitalocean.app/${data?.imgSmall})` }} />
          <div>
            <h1 class="text-3xl font-bold">{data?.title}</h1>
            <div className={styles.description}>
              {data?.description}
            </div>
          </div>
        </article>
      </>
    )
  }

  return (
    <div className={styles.row}>
      <FullContentBlog data={showArticle} />
      <div className={styles.articles}>
        <h2 className='text-3xl'>OTROS ARTÍCULOS</h2>
        <div className={styles.rowArticles}>
          {data?.length > 1 ?
            data?.map((item, index) => {
              return (
                <div className={styles.article} onClick={() => setShowArticle(item)}>
                  <div>
                    <div className={styles.imgSmallArticle} style={{ backgroundImage: `url(${item?.imgLg.src})` }} />
                  </div>
                  <p className={styles.articleTitle}>{item?.title}</p>
                </div>
              )
            })
            : <div>
              <p className={styles.notContent}>Sin contenido</p>
            </div>}
        </div>
      </div>
    </div>
  )
}