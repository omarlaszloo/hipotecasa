import fetData from '../../utils/AxiosServices'
import BlogComponent from './BlogComponent'

export default async function ContentBlog() {
  const blog = await fetData('blog/')
  return (
    <section id="blog">
      <BlogComponent data={blog} />
    </section>
  )
}