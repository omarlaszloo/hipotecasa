import Image from 'next/image'
import banks from './contants/banks'
import styles from './banks.module.css'

export default function Banks() {
  return (
    <section id="Bancos">
      <div className={styles.bank}>
        <div className={styles.row}>
          <h1 className="text-4xl">Bancos <br />con los que trabajamos</h1>
          <div className={styles.banksGrid}>
            {banks?.map(({ name, img }) => (
              <div className={styles.contentBank}>
                <Image
                  src={img}
                  className={styles.logoBacnk}
                  alt={name}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  )
}