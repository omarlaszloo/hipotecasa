import scotiabank from '../../../app/assets/images/bancos_sq135px_scotiabank.png'
import santander from '../../../app/assets/images/bancos_sq135px_santander.png'
import hsbc from '../../../app/assets/images/bancos_sq135px_hsbc.png'
import banorte from '../../../app/assets/images/bancos_sq135px_banorte.png'
import afirme from '../../../app/assets/images/bancos_sq135px_afirme.png'
import mifiel from '../../../app/assets/images/bancos_sq135px_mifel.png'
import bienbien from '../../../app/assets/images/bancos_sq135px_bienbien.png'
import bX from '../../../app/assets/images/bancos_sq135px_bX.png'
import casaexpress from '../../../app/assets/images/bancos_sq135px_casaexpress.png'
import smartlending from '../../../app/assets/images/bancos_sq135px_smartlending.png'
import heyBanco from '../../../app/assets/images/bancos_sq135px_heybanco.png'
import iban from '../../../app/assets/images/bancos_sq135px_iban.png'

const banks = [
  {
    name: 'Scotiabank',
    img: scotiabank,
  },
  {
    name: 'Santander',
    img: santander
  },
  {
    name: 'HSBC',
    img: hsbc,
  },
  {
    name: 'Banorte',
    img: banorte,
  },
  {
    name: 'Afirme',
    img: afirme,
  },
  {
    name: 'Mifiel',
    img: mifiel,
  },
  {
    name: 'bienbien',
    img: bienbien,
  },
  {
    name: 'bx+',
    img: bX,
  },
  {
    name: 'smartlending',
    img: smartlending,
  },
  {
    name: 'Hey,banco',
    img: heyBanco,
  },
  {
    name: 'casaexpress',
    img: casaexpress,
  },
  {
    name: 'iban',
    img: iban,
  }
]

export default banks