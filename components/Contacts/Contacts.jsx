import styles from './contact.module.css'

export default function Contacts() {
  return (
    <section id="Contacto">
      <div className={styles.contacts}>
        <div className={styles.row}>

          <div className={styles.contentContacts}>
            <div className={styles.columnDescription}>
              <div className={styles.info}>
                <h1 class="text-5xl font-bold">Dejanos tus comentarios</h1>
                <p class="py-6">Dudas sobre el tema, aclaraciones.<br />
                  Nuestro equipo se pondra en contacto contigo.</p>
              </div>
            </div>
            <div className={styles.form}>
              <div className={styles.rowForm}>
                <input type="text" className={styles.input} name="name" placeholder='Nombre' />
                <input type="text" className={styles.input} name="email" placeholder='Correo' />
              </div>
              <div className={styles.formReason}>
                <input text="text" className={styles.input} name="reason" placeholder='Asunto' />
                <textarea className={styles.input} name="message" placeholder='Mensaje' rows={10} />
              </div>
              <div className={styles.rowBtnContacts}>
                <button className="btn btn-neutral">Enviar</button>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
  )
}