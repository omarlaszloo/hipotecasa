export { default as Navigation } from './Navigation/Navigation';
export { default as Footer } from './Footer/Footer';
export { default as ListMain } from './ListMain/ListMain';
export { default as Contacts } from './Contacts/Contacts';
export { default as Tools } from './Tools/Tools';
export { default as Banks } from './Banks/Banks';
export { default as Products } from './Products/Products';
export { default as Requirements } from './Requirements/Requirements';
// export { default as ContentBlog } from './ContentBlog/ContentBlog';



