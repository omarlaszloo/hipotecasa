'use client'

import React, { useState } from 'react'
import Image from 'next/image'
import productImage from '../../app/assets/images/producto1_creditohipotecario.jpeg'
import styles from './products.module.css'
import ModalComponent from './ModalComponent'

export default function ProductsComponent({ products }) {
  const [data, setData] = useState()

  const handleOpenModal = (item) => {
    console.log(':: item', item)
    setData(item)
    document.getElementById('my_modal_5').showModal()
  }

  return (
    <>

      {products?.map((item, key) => (
        <div className={styles.card} key={key}>
          <div className={styles.contentimage}>
            <div className={styles.img}>
              <div className={styles.imgBg} style={{ backgroundImage: `url(https://octopus-app-mpo3n.ondigitalocean.app${item?.img})` }} />
            </div>
          </div>
          <div className={styles.descriptionCard}>
            <div className={styles.titleCard}>
              <p className="py-1">{item?.title || ''}</p>
            </div>
            <div className={styles.btnActions}>
              <div className={styles.btnInfo}>
                <button
                  className="btn"
                  onClick={() => handleOpenModal(item)}>
                  Ver más
                </button>
              </div>
            </div>
          </div>
        </div>
      ))}

      <ModalComponent data={data}>
        <div className="hero-content flex-col lg:flex-row">
          <Image
            src={data?.img ? `https://octopus-app-mpo3n.ondigitalocean.app/${data?.img}` : productImage}
            className="max-w-sm rounded-lg shadow-2xl"
            width={200} height={50}
          />
          <div>
            <h1 className="text-3xl font-bold" style={{ paddingBottom: 0 }}>{data?.title}</h1>
            <p className="py-6">{data?.description}</p>
          </div>
        </div>
      </ModalComponent>
    </>
  )
}