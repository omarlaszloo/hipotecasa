export default function ModalComponent({ children, data = {} }) {
  return (
    <dialog id="my_modal_5" className="modal">
      <div className="modal-box w-11/12 max-w-3xl">
        <form method="dialog">
          <button className="btn btn-sm btn-circle btn-ghost absolute right-2 top-2">✕</button>
        </form>
        {children}
      </div>
    </dialog>
  )
}