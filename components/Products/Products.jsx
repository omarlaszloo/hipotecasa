
import ProductsComponent from './ProductsComponent'
import styles from './products.module.css'
import fetData from '../../utils/AxiosServices'

const { ADMIN_API_APPLICASA } = process.env;

export default async function Products() {
  const DOMAIN_PROD = 'https://octopus-app-mpo3n.ondigitalocean.app/'
  const response = await fetch(`${DOMAIN_PROD}products/`);
  const products = await response?.json();

  return (
    <section id="Productos">
      <div className={styles.products}>
        <div className={styles.row}>
          <h1 className="text-4xl">Nuestros <br />productos</h1>
          <div className={styles.rowGrid}>
            <ProductsComponent products={products} />
          </div>
        </div>
      </div>
    </section>
  )
}