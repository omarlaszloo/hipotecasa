import RequirementsComponent from './RequirementsComponent'
import fetData from '../../utils/AxiosServices'

export default async function Requirements() {

  const DOMAIN_PROD = 'https://octopus-app-mpo3n.ondigitalocean.app/'
  const response = await fetch(`${DOMAIN_PROD}requirements/`);
  const requirements = await response.json();
  return <RequirementsComponent requirements={requirements} />
}