'use client'

import { useState } from 'react'
import {
  faCircleCheck
} from "@fortawesome/free-solid-svg-icons";
import ModalComponent from '../Modal/ModalComponent'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styles from './requirements.module.css'

export default function RequirementsComponent({ requirements }) {

  const [stepActive, setStepActive] = useState('step-primary')
  const [lineNumber, setLineNumber] = useState(1)
  const [items, setItems] = useState()

  const handleSteps = (nextStep, item) => {
    setItems(item)
    document.getElementById('my_modal_4').showModal()
    setLineNumber(nextStep)
  }
  return (
    <>
      <div className={styles.rowTitle}>
        <h1 className="text-4xl">Requisitos</h1>
      </div>
      <div className={styles.rowRequirements}>
        <div className={styles.main}>
          <div className={styles.lineSteps}>
            <ul className="steps">
              {requirements?.map((data, key) => {
                if (data?.isActive) {
                  return (
                    <li
                      data-content="✓"
                      className={`step ${key + 1 === lineNumber ? stepActive : ''} ${styles.stepSize}`}
                      onClick={() => handleSteps(key + 1, data?.main)}
                    >
                      <div className={`${styles.contentDot}`}>
                        <div className={`${styles.circle} ${key + 1 === lineNumber ? styles.bgColor : ''}`}>
                          <div className={styles.contentCircle}>
                            <p>{key + 1}</p>
                          </div>
                        </div>
                      </div>
                      <p className={styles.lineTitle}>{data.title}</p>
                      <div className={styles.subtitle}>
                        <p className={styles.lineSubTitle}>{data?.subtitle}</p>
                      </div>
                    </li>
                  )
                }
              })}
            </ul>
          </div>
        </div>
      </div>
      <ModalComponent>
        <div className="hero-content flex-col lg:flex-row">
          <ul className={styles.itemDescriptions}>
            {items?.map((data) => {
              if (data.isActive) {
                return (
                  <li>
                    <div className={styles.checks}>
                      <div className={styles.iconChek}>
                        <FontAwesomeIcon
                          icon={faCircleCheck}
                          className="fas fa-check"
                          style={{ color: '#00695C', fontSize: 25 }}
                        />
                      </div>
                      <div className={styles.itemLabels}>
                        <p>{data?.description}</p>
                      </div>
                    </div>
                  </li>
                )
              }
            })}
          </ul>
        </div>
      </ModalComponent>
    </>
  )
}