'use client'

import React from 'react'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { typeIcon } from './contants/tools'
import styles from './Tools.module.css'

export default function ToolsComponent({ tools }) {
  return (
    <div id="Herramientas" className={styles.helpTool}>
      <div className={styles.row}>
        <h1 className={`text-4xl  ${styles.title}`}>Nuestras <br />herramientas</h1>
        <div className={styles.contentTools}>
          <div className={tools?.length <= 5 ? styles.arraymd : styles.tools}>
            {tools?.map(({ title, description, link, icons, button, isActive }) => (
              isActive ? (
                <div className={styles.toolsCard} >
                  <Link href={link} target="_blank" className={styles.link}>
                    <div
                      className={styles.cardIcon}
                    >
                      <FontAwesomeIcon
                        icon={typeIcon[icons]}
                        className={`fas fa-check ${styles.faSmall}`}
                        style={{ color: '#607D8B' }}
                      />
                    </div>
                    <div className={styles.cardDescription}>
                      <p className={styles.toolLabel}>
                        {title || ''}
                      </p>
                      <p className={styles.toolDescription}>
                        {description || ''}
                      </p>
                    </div>
                    <div className={styles.toolsBtn}>
                      <button className='btn btn-xs sm:btn-xs md:btn-xs lg:btn-sm'>{button}</button>
                    </div>
                  </Link>
                </div>
              ) : null
            ))}
          </div>
        </div>
      </div>
    </div >
  )
}