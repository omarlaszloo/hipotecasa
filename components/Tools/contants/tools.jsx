import {
  faWandMagicSparkles,
  faCalculator,
  faUserShield,
  faUserNinja,
  faScaleBalanced,
} from "@fortawesome/free-solid-svg-icons";

export const typeIcon = {
  "faWandMagicSparkles": faWandMagicSparkles,
  "faScaleBalanced": faScaleBalanced,
  "faUserShield": faUserShield,
  "faCalculator": faCalculator,
  "faUserNinja": faUserNinja,
}

const tools = [
  {
    icon: faWandMagicSparkles,
    label: 'Simulador',
    description: 'Podras realizar una cotizacion de tu casa',
    bgColor: "#283593",
    link: 'https://simulador.applicasas.com/creditos2/create'
  },
  {
    icon: faScaleBalanced,
    label: 'Comparativo',
    description: 'Realiza un compartivo condiciones de credito',
    bgColor: '#FF8F00',
    link: 'https://simulador.applicasas.com/creditos2/create'
  },
  {
    icon: faUserShield,
    label: 'Perfilarme',
    description: 'Podras perfilarte aqui',
    bgColor: '#00796B',
    link: 'https://simulador.applicasas.com/perfilador/create'
  },
  {
    icon: faCalculator,
    label: 'Calcular',
    description: 'Realizar el calculo del monto de tu credito',
    bgColor: '#546E7A',
    link: 'https://simulador.applicasas.com/perfilador/create'
  },
  {
    icon: faUserNinja,
    label: 'Iniciar mi trámite',
    description: 'Inicia tu tramite aqui',
    bgColor: "#8E24AA",
    link: 'realiza tu tramite aqui'
  }
]

export default tools