import ToolsComponent from './ToolsComponent';
import fetData from '../../utils/AxiosServices'

export default async function Tools() {
  const DOMAIN_PROD = 'https://octopus-app-mpo3n.ondigitalocean.app/'
  const response = await fetch(`${DOMAIN_PROD}tools/`);
  const tools = await response.json();

  return (
    <section id="Herramientas">
      <ToolsComponent tools={tools} />
    </section>
  )
}