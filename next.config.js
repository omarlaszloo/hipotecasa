/** @type {import('next').NextConfig} */
const nextConfig = {
  crossOrigin: 'anonymous',
  reactStrictMode: true,
  experimental: { serverActions: true },
  images: {
    domains: ['127.0.0.1', 'localhost', 'http://127.0.0.1:8000/', 'octopus-app-mpo3n.ondigitalocean.app'],
  },

}

module.exports = nextConfig
