export default function fetData(url) {
  const DOMAIN_PROD = 'https://octopus-app-mpo3n.ondigitalocean.app/'
  const DOMAIN_DEV = 'http://127.0.0.1:8000/'
  return fetch(`${DOMAIN_PROD}/${url}/`, {
    mode: 'no-cors',
    cache: "force-cache",
    headers: {
      "Content-type": "json/application",
      "Accept": "application/json"
    },
  })
    .then(res => {
      if (!res.ok) {
        throw new Error('Error en la solicitud');
      }
      const contentType = res.headers.get('content-type');
      if (!contentType || !contentType.includes('application/json')) {
        throw new Error('La respuesta no es un JSON válido');
      }
      console.log(':: 111')
      return res.json();
    })
    .then(data => {
      // Hacer algo con los datos JSON
      console.log(':: data', data);
    })
    .catch(error => {
      // Manejar cualquier error
      console.error(':: ERROR', error);
    });
}